﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class HideBullet: MonoBehaviour
{
    public float speed;
    public float timeTorcid;
 
    private float currentTimeTorcid=0;
 
   
    // Update is called once per frame
    void Update () {
        currentTimeTorcid += Time.deltaTime;
        if(timeTorcid>currentTimeTorcid){
            transform.Translate(speed * Time.deltaTime,0,0);
        }else{
            Destroy(this.gameObject);
        }
    }
 
    public void OnTriggerEnter2D(Collider2D other){
        Debug.Log("Soy trigger");
        if (other.tag == "Finish") {
            Destroy (gameObject);
        }
    }
 
}