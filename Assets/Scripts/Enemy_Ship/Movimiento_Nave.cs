﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento_Nave : MonoBehaviour
{
    public int speed;
    public float time;
    private float currentTime;
    public GameObject laser;

    // Update is called once per frame
    void Update(){
     currentTime += Time.deltaTime;


       movimiento();

    }

    public void movimiento(){

        if(currentTime<time){
           transform.Translate(-speed*Time.deltaTime,0,0);
       }else{
           StartCoroutine(Pararnave());
       }
    
    }
    public void Instancialaser(){
        if (currentTime<time){
        Instantiate(laser,transform.position,Quaternion.identity,null);
        }
    }


    

    IEnumerator Pararnave(){
        //Parar la nave
        transform.position= new Vector3(transform.position.x,transform.position.y,0);
        
        yield return new WaitForSeconds(1f);
        
        currentTime = 0;
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Finish"){
            Destroy(this.gameObject);
        }
    }
}